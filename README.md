# WTR Weather Pro Landing Page
Improved and redesigned landing page of the WTR Weather Pro app by Felgo GmbH

 - [View original](https://felgo.com/weather-app/) landing page
 - [View improved](https://weather-pro.netlify.com/) landing page

## Technology Stack
The landing page is built in ReactJS v16 and Gatsby v2, and managed with Yarn.
The following main libraries are used:

 - Bootstrap 4
 - Fort Awesome
 - Gatsby Image
 - Node Sass

## Deployment
The landing page is deployed on Netlify.

## Bugs
Styling ``transition: height  9999999s;`` applied to the hero section to avoid automatic ``100vh`` resizing on mobile due to URL bar disappearing upon scrolling.

## Acknowledgments
The following code snippets were reused and modified:

 - [Bootstrap Speech Bubble Testimonial Carousel ](https://www.tutorialrepublic.com/snippets/preview.php?topic=bootstrap&file=speech-bubble-testimonial-carousel)
 - [Bootstrap 4 Sign up for Newsletter](https://bbbootstrap.com/snippets/sign-up-for-newsletter-86189414)