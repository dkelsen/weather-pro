import React, { Component } from "react"
import { Navbar, Nav } from "react-bootstrap"

import Image from "../image"
import smartphoneHeroImage from "../../../static/smartphone-hero.png"
import DownloadButtons from "../downloadButtons/index"

export default class Header extends Component {
  state = {
    classNameIfScrolled: ""
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    if (window.pageYOffset > 0 && window.screen.width > 992) {
        if (!this.state.classNameIfScrolled) {
          this.setState({ classNameIfScrolled: "navbar-scrolled" });   
        }
    } else {
        if (this.state.classNameIfScrolled) {
          this.setState({ classNameIfScrolled: "" });
        }
    }
  }

  render() {
    return (
      <>
        <Navbar collapseOnSelect expand="lg" id="mainNav" className={"navbar fixed-top " + this.state.classNameIfScrolled}>
          <div className="container">
            <Navbar.Brand href="#hero-section"><Image /></Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="ml-auto">
                <Nav.Link href="#features">Features</Nav.Link>
                <Nav.Link href="#preview">Preview</Nav.Link>
                <Nav.Link href="#testimonials">Testimonials</Nav.Link>
                <Nav.Link href="#download">Download</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </div>
        </Navbar>
        <header id="hero-section">
          <div className="container h-100 px-3">
            <div className="row text-center h-100 d-flex align-items-center">
              <div className="col-12 col-lg-6 order-lg-2">
                <div className="row">
                  <div className="col-12 mt-4 mt-lg-0">
                    <h1>WTR - Weather Pro</h1>
                    <h4>Forecast &amp; History</h4>
                  </div>
                  <div className="col-12 mt-3">
                    <p>
                      Explore up to 60 years of historical weather data at a glance, with rain quantity, 
                      probability, and sunshine hours included!
                    </p>
                  </div>
                  <DownloadButtons />
                </div>
              </div>
              <div className="col-12 col-lg-6 order-lg-1 mt-lg-5">
                <img src={smartphoneHeroImage} className="img-fluid" alt="Smartphone screenshot" />
              </div>
            </div>
          </div>
        </header>
      </>
    );
  }
}
