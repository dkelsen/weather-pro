import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import Carousel from "react-bootstrap/Carousel"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faStar } from "@fortawesome/free-solid-svg-icons"

export default () => (
  <div id="myCarousel" className="carousel slide" data-ride="carousel">
    {/* Carousel indicators */}
    <ol className="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>   
    {/* Wrapper for carousel items */}
    <Carousel controls={false} interval={null}>
      <Carousel.Item>
        <div className="row">
          <div className="col-sm-6">
            <div className="testimonial-wrapper">
              <p className="testimonial">
                I've always wanted to look back in time, like which days were sunny 
                last week for eg. I'm very happy with this valuable feature!
              </p>
              <div className="media">
                <div className="media-left d-flex mr-3">
                  <UserImage username="arthurGuy"/>									
                </div>
                <div className="media-body">
                  <div className="overview">
                    <div className="name"><b>Arthur Guy</b></div>
                    <div className="star-rating">
                      <ul className="list-inline">
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                      </ul>
                    </div>
                  </div>										
                </div>
              </div>
            </div>								
          </div>
          <div className="col-sm-6">
            <div className="testimonial-wrapper">
              <p className="testimonial">
                Very good app... fast, accurate and also has weather history available!
              </p>
              <div className="media">
                <div className="media-left d-flex mr-3">
                  <UserImage username="reinhardMatheis"/>						
                </div>
                <div className="media-body">
                  <div className="overview">
                    <div className="name"><b>Reinhard Matheis</b></div>
                    <div className="star-rating">
                      <ul className="list-inline">
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>								
          </div>
        </div>			
      </Carousel.Item>
      <Carousel.Item>
        <div className="row">
          <div className="col-sm-6">
            <div className="testimonial-wrapper">
              <p className="testimonial">
                Love seeing weather history!
              </p>
              <div className="media">
                <div className="media-left d-flex mr-3">
                  <UserImage username="tiegLaskowske"/>										
                </div>
                <div className="media-body">
                  <div className="overview">
                    <div className="name"><b>Tieg Laskowske</b></div>										
                    <div className="star-rating">
                      <ul className="list-inline">
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>								
          </div>
          <div className="col-sm-6">
            <div className="testimonial-wrapper">
              <p className="testimonial">
                I rely on this app to tell me how much rain to expect at different times of day, as well as 
                how much rain did occur at different times of day in the recent past. Invaluable!
              </p>
              <div className="media">
                <div className="media-left d-flex mr-3">
                  <UserImage username="stephenLancaster"/>									
                </div>
                <div className="media-body">
                  <div className="overview">
                    <div className="name"><b>Stephen Lancaster</b></div>
                    <div className="star-rating">
                      <ul className="list-inline">
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                        <li className="list-inline-item"><FontAwesomeIcon icon={ faStar }/></li>
                      </ul>
                    </div>
                  </div>										
                </div>
              </div>
            </div>								
          </div>
        </div>			
      </Carousel.Item>
    </Carousel>
  </div>
)

const UserImage = ({ username }) => {
  const data = useStaticQuery(graphql`
    query {
      tiegLaskowske: file(relativePath: { eq: "tieg-laskowske.jpg" }) {
        childImageSharp {
          fixed(width: 75, height: 75) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      arthurGuy: file(relativePath: { eq: "arthur-guy.jpg" }) {
        childImageSharp {
          fixed(width: 75, height: 75) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      reinhardMatheis: file(relativePath: { eq: "reinhard-matheis.jpg" }) {
        childImageSharp {
          fixed(width: 75, height: 75) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      stephenLancaster: file(relativePath: { eq: "stephen-lancaster.jpg" }) {
        childImageSharp {
          fixed(width: 75, height: 75) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return <Img fixed={data[username].childImageSharp.fixed} alt={username}/>
}