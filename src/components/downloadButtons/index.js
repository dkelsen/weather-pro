import React from "react"

const DownloadButtons = () => {
  const showButton = (deviceOS) => {
    let isMobile = /Android|iPhone/i.test(navigator.userAgent);
    return isMobile ? new RegExp(deviceOS).test(navigator.userAgent) : true; // Always show on Desktop
  }

  const PlayStoreButton = () => (
    <a href="https://play.google.com/store/apps/details?id=net.vplay.demos.apps.weatherapp&hl=en">
      <img className="img-fluid playstore-button" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png"/>
    </a>     
  )
  
  const AppStoreButton = () => (
    <a href="https://apps.apple.com/us/app/weather-pro-forecast-history/id1438432642">
      <img className="img-fluid appstore-button my-2" alt="Download on the App Store" src="https://v.fastcdn.co/u/c78db2cd/42115645-0-badgeappstore2.png"/>
    </a>
  )

  return (
    <>
      <div className="col-12">
        {/* Don't run "showButton" function during server side rendering */}
        { typeof window !== "undefined" ? ( showButton("Android") ? <PlayStoreButton /> : "" ) : "" }
      </div>
      <div className="col-12">
        { typeof window !== "undefined" ? ( showButton("iPhone") ? <AppStoreButton /> : "" ) : "" } 
      </div>
    </>
  )
}

export default DownloadButtons;