import React from "react"

const SubscribeForm = () => (
  <div class="newsletter py-5 px-3 px-lg-5 mb-5">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="bbb_newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
            <div class="bbb_newsletter_title_container">
              <div class="bbb_newsletter_icon">
                <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1560999634/send.png" alt="" />
              </div>
              <h4 class="bbb_newsletter_title">Stay up to date on our latest features!</h4>
            </div>
            <div class="bbb_newsletter_content clearfix">
              <form action="#" class="bbb_newsletter_form">
                <input type="email" class="bbb_newsletter_input" required="required" placeholder="Enter your email address" />
                <button class="bbb_newsletter_button">Subscribe</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default SubscribeForm;