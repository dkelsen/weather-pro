import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Testimonials from "../components/testimonials/index"
import DownloadButtons from "../components/downloadButtons/index"
import SubscribeForm from "../components/subscribeForm/index"

import binoculars from "../../static/binoculars.png"
import weather from "../../static/sun-rain.png"
import hourglass from "../../static/hourglass.png"

export default () => (
  <Layout>
    <SEO title="Home" />
    <section id="features">
      <div className="container px-3">
        <div className="row text-center">
          <div className="col-12 mb-4">
            <h2>The New Standard Of Weather Data</h2>
          </div>
        </div> 
        <div className="row text-center">
          <div className="col-12 col-lg-4 mb-4">
            <img src={binoculars} className="img-fluid features-icon" alt="Binoculars"/>
            <h4>Accurate 7 Day Forecast</h4>
            <p>Forecast for the next 7 days for all your favorite locations on a single page</p>
          </div>
          <div className="col-12 col-lg-4 mb-4">
            <img src={weather} className="img-fluid features-icon" alt="Clouds and sun"/>
            <h4>Hourly Forecast For Each Day</h4>
            <p>Daily data including hourly rain amount, rain possibility and sunshine hours</p>
          </div>
          <div className="col-12 col-lg-4">
            <img src={hourglass} className="img-fluid features-icon" alt="Clouds and sun"/>
            <h4>Historical Weather Information</h4>
            <p>Timeline feature reaches back 60 years for locations worldwide</p>
          </div>
        </div>
      </div>
    </section>
    <section id="preview">
      <div className="container px-3">
        <div className="row text-center">
          <div className="col-12 mb-4">
            <h2>See How It Works</h2>
          </div>
          <div className="col-12 mb-3 mb-lg-0">
            <YouTubeVideo />
          </div>
        </div>
      </div>
    </section>
    <WaveTransition />
    <section id="testimonials">
      <div className="container px-3">
        <div className="row text-center">
          <div className="col-12 mb-4">
            <h2>What Our Users Say</h2>
          </div>
          <div className="col-12 mb-3 mb-lg-0">
            <Testimonials />
          </div>
        </div>
      </div>
    </section>
    <section id="download">
      <div className="container px-3">
        <div className="row">
          <div className="col-12">
            <SubscribeForm />
          </div>
        </div>
        <div className="row text-center">
          <div className="col-12">
            <h2>Simple. Accurate. WTR - Weather Pro</h2>
          </div>
          <p className="col-12 mb-4">
            Now available for iPhone and Android devices
          </p>
          <DownloadButtons />
        </div>
      </div>
    </section>
    <footer className="text-center py-3">
      <p>© FELGO GmbH { new Date().getFullYear() }</p>
    </footer>
  </Layout>
)

const WaveTransition = () => (
  <svg className="wave-transition" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
    <path fill="#cce5ff" fill-opacity="1" d="M0,192L40,208C80,224,160,256,240,229.3C320,203,400,117,480,80C560,43,640,53,720,69.3C800,85,880,107,960,138.7C1040,171,1120,213,1200,234.7C1280,256,1360,256,1400,256L1440,256L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path>
    <path fill="#75b5e1" fill-opacity="1" d="M0,160L48,154.7C96,149,192,139,288,165.3C384,192,480,256,576,240C672,224,768,128,864,101.3C960,75,1056,117,1152,128C1248,139,1344,117,1392,106.7L1440,96L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path>
  </svg>
)

const YouTubeVideo = () => (
  <div className="embed-responsive embed-responsive-16by9">
    <iframe 
      title="Weather Pro preview video" 
      className="embed-responsive-item" 
      src="https://www.youtube.com/embed/VinJJCNqUh4" 
      allowFullScreen>
    </iframe>
  </div>
)
